package multiplicationQuiz;


public class MultiplicationQuiz {

    public void start() {
        System.out.println("Start is running...");
        int score = 0;

        for (int i = 0; i < 5; i++) {
            int ranNumOne = (int)Math.floor(Math.random() * 10 + 1);
            int ranNumTwo = (int)Math.floor(Math.random() * 10 + 1);
            System.out.println("What is " + ranNumOne + " * " + ranNumTwo + "?");
            int answer = Integer.parseInt(Keyboard.readInput());

            if (answer == ranNumOne * ranNumTwo){
                System.out.println("Correct!!!");
                score += 1;
            } else {
                System.out.println("Incorrect");
            }

        }
        System.out.println("Game over, you scored " + score
                + " points!");


    }

    public static void main(String[] args) {
        MultiplicationQuiz p = new MultiplicationQuiz();
        p.start();

    }

}

